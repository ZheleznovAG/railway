﻿using UnityEngine;

namespace ProjectTest
{
    public enum AnchorPresets
    {
        TopLeft,
        TopCenter,
        TopRight,

        MiddleLeft,
        MiddleCenter,
        MiddleRight,

        BottomLeft,
        BottomCenter,
        BottomRight,
        BottomStretch,

        VertStretchLeft,
        VertStretchRight,
        VertStretchCenter,

        HorStretchTop,
        HorStretchMiddle,
        HorStretchBottom,

        StretchAll
    }

    public enum PivotPresets
    {
        TopLeft,
        TopCenter,
        TopRight,

        MiddleLeft,
        MiddleCenter,
        MiddleRight,

        BottomLeft,
        BottomCenter,
        BottomRight,
    }

    public static class Library
    {
        #region SetParent Transform Custom
        public static void SetParentPrototype(this Transform source, Transform target) {
            source.parent = target.transform;
            source.localPosition = Vector3.zero;
            source.localEulerAngles = Vector3.zero;
            source.localScale = Vector3.one;
        }
        public static void SetParentPrototype<T>(this T source, Transform target) where T : Component
        {
            SetParentPrototype(source.gameObject.transform, target);
        }
        public static void SetParentPrototype<C>(this GameObject source, C target) where C : Component
        {
            SetParentPrototype(source.transform, target.gameObject.transform);
        }
        public static void SetParentPrototype<T, C>(this T source, C target) where T : Component where C : Component
        {
            SetParentPrototype(source.gameObject.transform, target.gameObject.transform);
        }
        public static void SetParentPrototype(this GameObject source, Transform target) {
            SetParentPrototype(source.transform, target);
        }
        public static void SetParentPrototype(this GameObject source, GameObject target)
        {
            SetParentPrototype(source.transform, target.transform);
        }
        #endregion
        #region Set Material and Color MeshRenderer
        public static void SetMaterialPrototype(this GameObject go, Color color, string strShader = "Standard")
        {
            MeshRenderer mr = go.GetComponent<MeshRenderer>();

            mr?.SetMaterialPrototype(color, strShader);
        }

        public static void SetMaterialPrototype(this GameObject go, string strHexColor = "", string strShader = "Standard")
        {
            MeshRenderer mr = go.GetComponent<MeshRenderer>();

            mr?.SetMaterialPrototype(strHexColor, strShader);

        }

        public static void SetMaterialPrototype(this MeshRenderer mr, Color color, string strShader = "Standard")
        {
            mr.SetMaterialPrototypeMain(color, strShader);
        }

        public static void SetMaterialPrototype(this MeshRenderer mr, string strHexColor = "", string strShader = "Standard") {
            
            Color color;

            if (ColorUtility.TryParseHtmlString(strHexColor, out color))
            {
                mr.SetMaterialPrototypeMain(color, strShader);
            }
        }

        static void SetMaterialPrototypeMain(this MeshRenderer mr, Color color, string strShader) {

            mr.material = new Material(Shader.Find(strShader));
            mr.material.color = color;
        }
        #endregion
        #region Inertian
        public static void Tilt2(float accelerate_in, float accelerate_out, float amplitude_velocity, float to_value, ref float value, ref float velocity)
        {
            ToValue(accelerate_in, accelerate_out, Mathf.Clamp01(Mathf.Abs(to_value - value)), ref velocity);
            ToValue(Mathf.Abs(velocity) * amplitude_velocity, Mathf.Abs(velocity) * amplitude_velocity, to_value, ref value);
        }
        public static bool IsNoNull<T>(T t)
        {
            bool flag = false;
            if (t != null)
            {
                flag = true;
            }
            return flag;
        }

        public static bool? IsPositiveValue(float value)
        {
            bool? flag = null;

            if (value > 0f)
            {
                flag = true;
            }
            else if (value < 0f)
            {
                flag = false;
            }
            else
            {
                flag = null;
            }

            return flag;
        }
        public static void ToValueVector(float koef, float to_value, Vector3 mask, ref Vector3 value)
        {
            float x = value.x;
            float y = value.y;
            float z = value.z;

            if (mask == Vector3.forward)
            {
                ToValue(koef, to_value, ref z);
            }
            else if (mask == Vector3.right)
            {
                ToValue(koef, to_value, ref x);
            }
            else if (mask == Vector3.up)
            {
                ToValue(koef, to_value, ref y);
            }
            value.Set(x, y, z);
        }
        public static void ToVectorVector(Vector3 koef_in, Vector3 koef_out, Vector3 to_value, ref Vector3 value)
        {
            float x = value.x;
            float y = value.y;
            float z = value.z;
            ToValue(koef_in.x, koef_out.x, to_value.x, ref x);
            ToValue(koef_in.y, koef_out.y, to_value.y, ref y);
            ToValue(koef_in.z, koef_out.z, to_value.z, ref z);
            value.Set(x, y, z);
        }
        public static bool ToValue(float koef, float to_value, ref float value)
        {
            if (isAreaEps(to_value, value) == false)
            {
                if (value > to_value)
                {
                    value -= koef * Time.deltaTime;
                    if (value < to_value)
                    {
                        value = to_value;
                    }
                }
                else if (value < to_value)
                {
                    value += koef * Time.deltaTime;
                    if (value > to_value)
                    {
                        value = to_value;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ToValue(float koef_in, float koef_out, float to_value, ref float value)
        {
            if (isAreaEps(to_value, value) == false)
            {
                if (isAreaEps(to_value, 0f))
                {
                    if (0f < value)
                    {
                        value -= koef_out * Time.deltaTime;
                        if (value < to_value)
                        {
                            value = 0f;
                        }
                    }
                    else if (0f > value)
                    {
                        value += koef_out * Time.deltaTime;
                        if (value > to_value)
                        {
                            value = 0f;
                        }
                    }
                }
                else
                {
                    if (to_value > 0 && to_value < value)
                    {
                        value -= koef_out * Time.deltaTime;
                        if (value < to_value)
                        {
                            value = to_value;
                        }
                    }
                    else if (to_value > 0 && to_value > value)
                    {
                        value += koef_in * Time.deltaTime;
                        if (value > to_value)
                        {
                            value = to_value;
                        }
                    }
                    else if (to_value < 0 && to_value < value)
                    {
                        value -= koef_in * Time.deltaTime;
                        if (value < to_value)
                        {
                            value = to_value;
                        }
                    }
                    else if (to_value < 0 && to_value > value)
                    {
                        value += koef_out * Time.deltaTime;
                        if (value > to_value)
                        {
                            value = to_value;
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void ToZero(float damper_positive, float damper_negative, ref float value)
        {
            if (value > 0f)
            {
                value -= damper_negative * Time.deltaTime;
                if (value < 0f)
                {
                    value = 0f;
                }
            }
            else if (value < 0f)
            {
                value += damper_positive * Time.deltaTime;
                if (value > 0f)
                {
                    value = 0f;
                }
            }
        }
        public static void ToZeroVector(float damper_positive, float damper_negative, Vector3 mask, ref Vector3 value)
        {
            float x = value.x;
            float y = value.y;
            float z = value.z;

            if (mask == Vector3.forward)
            {
                ToZero(damper_positive, damper_negative, ref z);
            }
            else if (mask == Vector3.right)
            {
                ToZero(damper_positive, damper_negative, ref x);
            }
            else if (mask == Vector3.up)
            {
                ToZero(damper_positive, damper_negative, ref y);
            }
            value.Set(x, y, z);
        }
        public static bool isAreaEps(float a, float b)
        {
            bool flag = false;
            if (Mathf.Abs(a - b) < 1E-3f)
            {
                flag = true;
            }
            return flag;
        }
        #endregion
        #region RectTransfrom anchor, pivot and offset
        public static void SetOffsetZero(this RectTransform source) {
            source.offsetMax = Vector2.zero;
            source.offsetMin = Vector2.zero;
        }
        public static void SetAnchor(this RectTransform source, AnchorPresets allign, int offsetX = 0, int offsetY = 0)
        {
            source.anchoredPosition = new Vector3(offsetX, offsetY, 0);

            switch (allign)
            {
                case (AnchorPresets.TopLeft):
                    {
                        source.anchorMin = new Vector2(0, 1);
                        source.anchorMax = new Vector2(0, 1);
                        break;
                    }
                case (AnchorPresets.TopCenter):
                    {
                        source.anchorMin = new Vector2(0.5f, 1);
                        source.anchorMax = new Vector2(0.5f, 1);
                        break;
                    }
                case (AnchorPresets.TopRight):
                    {
                        source.anchorMin = new Vector2(1, 1);
                        source.anchorMax = new Vector2(1, 1);
                        break;
                    }

                case (AnchorPresets.MiddleLeft):
                    {
                        source.anchorMin = new Vector2(0, 0.5f);
                        source.anchorMax = new Vector2(0, 0.5f);
                        break;
                    }
                case (AnchorPresets.MiddleCenter):
                    {
                        source.anchorMin = new Vector2(0.5f, 0.5f);
                        source.anchorMax = new Vector2(0.5f, 0.5f);
                        break;
                    }
                case (AnchorPresets.MiddleRight):
                    {
                        source.anchorMin = new Vector2(1, 0.5f);
                        source.anchorMax = new Vector2(1, 0.5f);
                        break;
                    }

                case (AnchorPresets.BottomLeft):
                    {
                        source.anchorMin = new Vector2(0, 0);
                        source.anchorMax = new Vector2(0, 0);
                        break;
                    }
                case (AnchorPresets.BottomCenter):
                    {
                        source.anchorMin = new Vector2(0.5f, 0);
                        source.anchorMax = new Vector2(0.5f, 0);
                        break;
                    }
                case (AnchorPresets.BottomRight):
                    {
                        source.anchorMin = new Vector2(1, 0);
                        source.anchorMax = new Vector2(1, 0);
                        break;
                    }

                case (AnchorPresets.HorStretchTop):
                    {
                        source.anchorMin = new Vector2(0, 1);
                        source.anchorMax = new Vector2(1, 1);
                        break;
                    }
                case (AnchorPresets.HorStretchMiddle):
                    {
                        source.anchorMin = new Vector2(0, 0.5f);
                        source.anchorMax = new Vector2(1, 0.5f);
                        break;
                    }
                case (AnchorPresets.HorStretchBottom):
                    {
                        source.anchorMin = new Vector2(0, 0);
                        source.anchorMax = new Vector2(1, 0);
                        break;
                    }

                case (AnchorPresets.VertStretchLeft):
                    {
                        source.anchorMin = new Vector2(0, 0);
                        source.anchorMax = new Vector2(0, 1);
                        break;
                    }
                case (AnchorPresets.VertStretchCenter):
                    {
                        source.anchorMin = new Vector2(0.5f, 0);
                        source.anchorMax = new Vector2(0.5f, 1);
                        break;
                    }
                case (AnchorPresets.VertStretchRight):
                    {
                        source.anchorMin = new Vector2(1, 0);
                        source.anchorMax = new Vector2(1, 1);
                        break;
                    }

                case (AnchorPresets.StretchAll):
                    {
                        source.anchorMin = new Vector2(0, 0);
                        source.anchorMax = new Vector2(1, 1);
                        break;
                    }
            }
        }

        public static void SetPivot(this RectTransform source, PivotPresets preset)
        {

            switch (preset)
            {
                case (PivotPresets.TopLeft):
                    {
                        source.pivot = new Vector2(0, 1);
                        break;
                    }
                case (PivotPresets.TopCenter):
                    {
                        source.pivot = new Vector2(0.5f, 1);
                        break;
                    }
                case (PivotPresets.TopRight):
                    {
                        source.pivot = new Vector2(1, 1);
                        break;
                    }

                case (PivotPresets.MiddleLeft):
                    {
                        source.pivot = new Vector2(0, 0.5f);
                        break;
                    }
                case (PivotPresets.MiddleCenter):
                    {
                        source.pivot = new Vector2(0.5f, 0.5f);
                        break;
                    }
                case (PivotPresets.MiddleRight):
                    {
                        source.pivot = new Vector2(1, 0.5f);
                        break;
                    }

                case (PivotPresets.BottomLeft):
                    {
                        source.pivot = new Vector2(0, 0);
                        break;
                    }
                case (PivotPresets.BottomCenter):
                    {
                        source.pivot = new Vector2(0.5f, 0);
                        break;
                    }
                case (PivotPresets.BottomRight):
                    {
                        source.pivot = new Vector2(1, 0);
                        break;
                    }
            }
        }
        #endregion
    }

}
