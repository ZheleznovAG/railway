﻿using System;
using UnityEngine;

namespace ProjectTest
{
    class ManagerCamera : MonoBehaviour
    {
        Transform pivotCamera3D;
        Camera camera3D;
        Camera cameraGUI;
        public DataCamera stateCamera { set; get; }
        Vector3 eulerAngelsFocusObject = Vector3.zero;
        Quaternion rotateFocusObject;


        public Camera Camera3D {
            private set { }
            get {
                return camera3D;
            }
        }

        public Camera CameraGUI
        {
            private set { }
            get
            {
                return cameraGUI;
            }
        }
        #region Init
        public void Init()
        {
            stateCamera = GameDefines.Instance.Data.CAMERA_FIXED;

            camera3D = Camera.main;
            pivotCamera3D = new GameObject("pivotCamera3D").transform;
            pivotCamera3D.SetParentPrototype(this);
            camera3D.SetParentPrototype(pivotCamera3D);
            camera3D.transform.localPosition = - Vector3.forward * stateCamera.OFFSET;
            Restart();

            cameraGUI = new GameObject("GUI Camera").AddComponent<Camera>();
            cameraGUI.SetParentPrototype(this);
            cameraGUI.clearFlags = CameraClearFlags.Depth;
            cameraGUI.cullingMask = 1 << 5;
            cameraGUI.orthographic = true;
        }
        #endregion


        Vector2 inputTouch = Vector2.zero;
        [SerializeField] Vector2 lookAt = Vector3.zero;
        [SerializeField] float rotationYZ, rotationZX;
        float ang_YZ_norm, ang_YZ_norm_target;
        //Transform target;

        //Vector3 directionCamera;

        internal void Restart()
        {
            stateCamera = GameDefines.Instance.Data.CAMERA_FIXED;
            lookAt = Vector2.zero;
            rotationYZ = stateCamera.DEFAULT_ANGLE_YZ;
            rotationZX = eulerAngelsFocusObject.y;
            pivotCamera3D.rotation = Quaternion.Euler(rotationYZ, rotationZX, 0.0F);
        }

        internal void UpdateFocusAttach()
        {
            Restart();
        }

        internal void SwitchCamera()
        {
            if (stateCamera == GameDefines.Instance.Data.CAMERA_FIXED)
            {
                rotationYZ = pivotCamera3D.localEulerAngles.x;
                rotationZX = pivotCamera3D.localEulerAngles.y;
                stateCamera = GameDefines.Instance.Data.CAMERA_FREE;
            }
            else
            {
                lookAt = Vector2.zero;
                stateCamera = GameDefines.Instance.Data.CAMERA_FIXED;
            }
        }
        public void UpdateRacursCamera(Train train, bool isMove)
        {
            UpdatePivotCamera(train.TransformTrain.localPosition);
            eulerAngelsFocusObject = train.TransformTrain.localEulerAngles;
            rotateFocusObject = train.TransformTrain.rotation;
        }

        public void UpdatePivotCamera(Vector3 vector)
        {
            pivotCamera3D.localPosition = vector;
        }

        public void UpdateRacursCameraInputLook(Vector2 vector)
        {
            if (stateCamera == GameDefines.Instance.Data.CAMERA_FREE)
            {
                inputTouch = Vector2.ClampMagnitude(vector, stateCamera.SMOOTH_INPUT);
            }
        }

        #region Legacy
        internal void LateUpdateCustom()
        {
            if (stateCamera == GameDefines.Instance.Data.CAMERA_FREE)
            {
                CalculateLook(
                    inputTouch.x,
                    stateCamera.SMOOTH_INPUT,
                    inputTouch.y,
                    stateCamera.SMOOTH_INPUT
                );
                CalculateRotationFree();
                SetRotation();
            }
            else if (stateCamera == GameDefines.Instance.Data.CAMERA_FIXED)
            {
                CalculateRotationFixed();
            }
        }

        private void CalculateRotationFixed()
        {
            pivotCamera3D.rotation = Quaternion.Slerp(
                pivotCamera3D.rotation,
                Quaternion.Euler(0f, eulerAngelsFocusObject.y, 0f)
                * Quaternion.Euler(stateCamera.DEFAULT_ANGLE_YZ, 0f, 0f),
                Time.deltaTime * stateCamera.ROTATE_KOEF_VELOCITY_YZ
            );
        }

        private void SetRotation()
        {
            if (rotationZX >= 360f)
            {
                rotationZX -= 360f;
            }
            else if (rotationZX < 0f) {
                rotationZX += 360f;
            }
            pivotCamera3D.rotation = Quaternion.Euler(rotationYZ, rotationZX, 0.0F);
        }
        void CalculateLook(float X_To, float X_Smooth, float Y_To, float Y_Smooth)
        {
            lookAt.x = Mathf.Lerp(lookAt.x, X_To / X_Smooth, Time.deltaTime);
            lookAt.y = Mathf.Lerp(lookAt.y, Y_To / Y_Smooth, Time.deltaTime);
            inputTouch = Vector2.zero;
        }
        void CalculateRotationFree()
        {
            rotationYZ -= lookAt.y * stateCamera.ROTATE_KOEF_VELOCITY_YZ;
            rotationYZ = Mathf.Clamp(rotationYZ, stateCamera.MINIMUM_YZ, stateCamera.MAXIMUM_YZ);
            rotationZX += lookAt.x * stateCamera.ROTATE_KOEF_VELOCITY_ZX;
        }
        #endregion
    }
}