﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data Camera", fileName = "data_camera_")]
    public class DataCamera : ScriptableObject
    {
        public float MINIMUM_YZ;
        public float MAXIMUM_YZ;
        public float ROTATE_KOEF_VELOCITY_YZ;
        public float ROTATE_KOEF_VELOCITY_ZX;
        public float DEFAULT_ANGLE_YZ;
        public float OFFSET;
        public float SMOOTH_INPUT;

    }
}