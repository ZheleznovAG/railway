﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectTest
{
    class Logic
    {
        Level level;
        InputControl inputControl;
        ManagerGUI managerGUI;
        ManagerCamera managerCamera;
        Model model;

        public Logic(Level level, ManagerGUI managerGUI, InputControl inputControl, ManagerCamera managerCamera)
        {
            this.level = level;
            this.managerGUI = managerGUI;
            this.inputControl = inputControl;
            this.managerCamera = managerCamera;
            model = new Model();
            Attach();
            model.UpdateIndicationInit();
        }
        #region Attach
        void Attach() {
            AttachCameraToGUI();
            AttachCamera3DToFocusObject();
            AttachInputControlToManagerCamera();
            ListenEventsButton();
            SetEventStantionDetectionInLevel();
            SetEventUpdatePostionTrain();
            SetEventUpdateIndicationValueInGUI();
            SetEventOverTime();
            SetEventShowHideItemGUI();
        }

        private void SetEventShowHideItemGUI()
        {
            model.eventIsAvailable += SetEventShowHideItemGUITakeCargo;
        }
        void SetEventShowHideItemGUITakeCargo(bool isAvailable) {
            managerGUI.IsShowHideItem(isAvailable, GameDefines.Instance.Data.BUTTON_TAKE_CARGO);
        }

        private void SetEventOverTime()
        {
            model.eventOverTime += OverTime;
        }

        private void SetEventUpdateIndicationValueInGUI()
        {
            model.eventUpdateIndicationValue += managerGUI.UpdateIndicationValue;
        }

        void AttachCameraToGUI()
        {
            managerGUI.AttachCameraToGUI(managerCamera.CameraGUI);
        }

        private void AttachCamera3DToFocusObject()
        {
            level.eventUpdatePositionTrain += managerCamera.UpdateRacursCamera;
            managerCamera.UpdateFocusAttach();
            level.EventUpdateFocusAttach();
        }

        private void SetEventStantionDetectionInLevel() {
            level.eventStantionDetection += model.ObjectInAreaStantion;
        }

        private void SetEventUpdatePostionTrain() {
            level.eventUpdatePositionTrain += model.UpdatePositionTrain;
        }

        private void AttachInputControlToManagerCamera()
        {
            inputControl.eventInputLook += managerCamera.UpdateRacursCameraInputLook;
        }

        void ListenEventsButton() {
            inputControl.eventClickButton += ActionOnEventClickButton;
        }

        #endregion
        #region Action By Click Button
        void ActionOnEventClickButton(DataButton dataTypeButton, bool isPressed) {
            if (dataTypeButton == GameDefines.Instance.Data.BUTTON_GO)
            {
                if (isPressed)
                {
                    level.InputControlObject(GameDefines.Instance.Data.TRAIN_GO);
                }
                else
                {
                    level.InputControlObject(GameDefines.Instance.Data.TRAIN_STOP);
                }
            }
            else if (dataTypeButton == GameDefines.Instance.Data.BUTTON_BEEP)
            {
                if (isPressed)
                {
                    level.InputControlObject(GameDefines.Instance.Data.TRAIN_BEEP);
                }
            }
            else if (dataTypeButton == GameDefines.Instance.Data.BUTTON_TAKE_CARGO)
            {
                if (isPressed)
                {
                    TakeCargo();
                }
            }
            else if (dataTypeButton == GameDefines.Instance.Data.BUTTON_SWITCH_CAMERA)
            {
                if (isPressed)
                {
                    managerCamera.SwitchCamera();
                }
            }
            else if (dataTypeButton == GameDefines.Instance.Data.BUTTON_RESTART)
            {
                if (isPressed)
                {
                    GameRestart();
                }
            }
        }

        void TakeCargo()
        {
            if (model.IsAvailableAction)
            {
                model.TakeCargo();
            }
        }

        void GameRestart()
        {
            managerGUI.ToWindow(GameDefines.Instance.Data.GUI_PLAY);
            level.Restart();
            model.Restart();
            managerCamera.Restart();
        }

        void OverTime() {
            managerGUI.ToWindow(GameDefines.Instance.Data.GUI_RESTART);
        }
        #endregion
        #region Update, LateUpdate
        internal void UpdateCustom()
        {
            if (model.stateGame.isPause == false)
            {
                level.UpdateCustom();
                model.UpdateTimer();
            }
        }

        internal void LateUpdateCustom()
        {
            if (model.stateGame.isPause == false)
            {
                level.LateUpdateCustom();
                managerCamera.LateUpdateCustom();
            }
        }
        #endregion
    }
}
