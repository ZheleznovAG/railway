﻿using UnityEngine;
using System.Collections;

namespace ProjectTest
{

    public class GameDefines : Singleton<GameDefines>
    {
        DataGameDefines dataGameDefines;

        public void Init()
        {
            Data = Resources.Load("dataGameDefines") as DataGameDefines;
            Data.fontDefault = Resources.GetBuiltinResource<Font>("Arial.ttf");
        }

        public DataGameDefines Data {
            get {
                return dataGameDefines;
            }
            private set {
                dataGameDefines = value;
            }
        }

    }

}
