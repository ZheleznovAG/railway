﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectTest
{
    class DetectionCollider : MonoBehaviour
    {
        public event Action<StantionDetection, bool> eventStantionDetection;
        public bool IsArea { private set; get; }
        public void Reset() { }

        void OnTriggerEnter(Collider other)
        {
            StantionDetection stantionDetection = other.gameObject.GetComponent<StantionDetection>();
            if (stantionDetection != null)
            {
                if (IsArea == false)
                {
                    IsArea = true;
                    eventStantionDetection?.Invoke(stantionDetection, IsArea);
                }
            }
        }
        void OnTriggerExit(Collider other)
        {
            StantionDetection stantionDetection = other.gameObject.GetComponent<StantionDetection>();
            if (stantionDetection != null)
            {
                if (IsArea == true)
                {
                    IsArea = false;
                    eventStantionDetection?.Invoke(stantionDetection, IsArea);
                }
            }
        }
    }
}