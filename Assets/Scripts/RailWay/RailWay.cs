﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using PathCreationEditor;
using PathCreation.Examples;

namespace ProjectTest
{

    public class RailWay : MonoBehaviour
    {
        public PathCreator pathCreator;
        RoadMeshCreator roadMeshCreator;

        public void Init() {
            CreateRailWayPath();
        }

        void CreateRailWayPath() {
            pathCreator = this.gameObject.AddComponent<PathCreator>();
            RailWayNodes railWayNodes = new RailWayNodes();
            BezierPath bezierPath = new BezierPath(
                new List<Vector2> {
                    4f * Vector2.down,
                    4f * new Vector2(0.5f, -0.5f),
                    4f * Vector2.right,
                    4f * new Vector2(0.5f, 0.5f),
                    4f * Vector2.up,
                    4f * new Vector2(-0.5f, 0.5f),
                    4f * Vector2.left,
                    4f * new Vector2(-0.5f, -0.5f),
                },
                true,
                PathSpace.xz
            );
            
            pathCreator.bezierPath = bezierPath;

            roadMeshCreator = this.gameObject.AddComponent<RoadMeshCreator>();
            roadMeshCreator.roadMaterial = (Material)Resources.Load("RailWay/RailWay");
            roadMeshCreator.undersideMaterial = (Material)Resources.Load("RailWay/RailWay Underside");
            roadMeshCreator.roadWidth = 0.04f;
            roadMeshCreator.thickness = 0.02f;
            roadMeshCreator.textureTiling = 256f;

            //roadMeshCreator.UpdateCustom();
            roadMeshCreator.pathCreator = pathCreator;
            roadMeshCreator.CreatePath();


        }
        public Vector3 GetPointAtDistance(float distance) {
            return pathCreator.path.GetPointAtDistance(distance);
        }
        public Vector3 GetDirectionAtDistance(float distance)
        {
            return pathCreator.path.GetDirectionAtDistance(distance);
        }


    }

}