﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data action", fileName = "dataAction")]
    public class DataActionTrain : ScriptableObject
    {
        public string type = "";

    }
}