﻿using System;
using UnityEngine;

namespace ProjectTest
{
    public class Stantion : MonoBehaviour
    {
        public int index { private set; get; }
        StantionDetection stantionDetection;

        public void Init(int index) {
            this.index = index;
            this.transform.localPosition -= 0.0199f * Vector3.up;
            CreateView();
            CreateZoneDetection();
        }

        void CreateView() {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.name = "builder";
            go.SetParentPrototype(this);
            go.SetMaterialPrototype(Color.cyan);
            Destroy(go.GetComponent<BoxCollider>());
            Transform tr = go.transform;
            tr.localScale = 0.3f * Vector3.one;

            tr.localPosition += GameDefines.Instance.Data.OFFSET_STANTION_BUILDER;
        }

        void CreateZoneDetection() {
            GameObject go = new GameObject("zoneDetection");
            go.SetParentPrototype(this);
            stantionDetection = go.AddComponent<StantionDetection>();
            stantionDetection.Init(this);
        }

    }
}