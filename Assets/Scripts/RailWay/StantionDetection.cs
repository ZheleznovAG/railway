﻿using System;
using UnityEngine;

namespace ProjectTest
{
    public class StantionDetection : MonoBehaviour
    {
        Stantion stantion;

        public void Init(Stantion stantion) {
            this.stantion = stantion;
            CreateComponents();
        }
        void CreateComponents() {
            CreateCollider();
            CreateViewZone();
        }

        private void CreateCollider()
        {
            //GameObject go = new GameObject("colliderDetection");
            //go.SetParentPrototype(this);
            BoxCollider boxCollider = this.gameObject.AddComponent<BoxCollider>();
            boxCollider.size = 0.5f * Vector3.one;
            boxCollider.center += 0.25f * Vector3.up;
        }

        private void CreateViewZone()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Plane);
            Destroy(go.GetComponent<MeshCollider>());
            go.name = "viewZoneDetection";
            go.SetParentPrototype(this);
            go.SetMaterialPrototype(Color.yellow);
            Transform tr = go.transform;
            //tr.localEulerAngles = 90f*Vector3.right;
            tr.localScale = 0.05f * Vector3.one;

            //tr.localPosition += GameDefines.Instance.Data.OFFSET_STANTION_BUILDER;
        }
    }
}