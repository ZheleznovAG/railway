﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ProjectTest {

    public class RailWayNodes
    {

        List<Vector3> nodes;
        int depthSearch = 3;

        public List<Vector3> Nodes
        {
            get
            {
                return nodes;
            }
            private set { }
        }

        public RailWayNodes() {
            //CreateNodes();
            //QWER();
        }

        private void QWER()
        {
            throw new NotImplementedException();
        }

        void CreateNodes() {
            bool[,] array_source = GetPointFromImageMap();

            QuadSearchWayPoins quadSearch = new QuadSearchWayPoins(array_source, depthSearch);
            
            
            
        }

        #region Load Image and Create Array Points
        bool[,] GetPointFromImageMap() {    
            Texture2D texture2D = LoadImageMap();
            bool[,] array_source = new bool[texture2D.width, texture2D.height];

            for (int i = 0; i < texture2D.width; i++)
            {
                for (int j = 0; j < texture2D.height; j++)
                {
                    if (texture2D.GetPixel(i, j) != Color.white)
                    {
                        array_source[i, j] = true;
                    }
                }
            }

            return array_source;
        }

        public Texture2D LoadImageMap()
        {
            return Texture2D.Instantiate((Texture2D)Resources.Load(GameDefines.Instance.Data.STR_PATH_IMAGE_MAP));
        }
        #endregion

    }

}
class ConteinerQuadSearch {
    public QuadSearchWayPoins a00, a01, a10, a11;
    public List<QuadSearchWayPoins> list;

    public ConteinerQuadSearch(QuadSearchWayPoins a00, QuadSearchWayPoins a01,
        QuadSearchWayPoins a10, QuadSearchWayPoins a11) {
        this.a00 = a00;
        this.a01 = a01;
        this.a10 = a10;
        this.a11 = a11;

        list = new List<QuadSearchWayPoins>();
    }
    public ConteinerQuadSearch()
    {
        list = new List<QuadSearchWayPoins>();
    }
}
class ConteinerQuarters
{
    public bool[,] a00, a01, a10, a11;
    public List<bool[,]> list;

    public ConteinerQuarters(ref bool[,] a00, ref bool[,] a01,
        ref bool[,] a10, ref bool[,] a11)
    {
        this.a00 = a00;
        this.a01 = a01;
        this.a10 = a10;
        this.a11 = a11;

        list = new List<bool[,]>();
        list.Add(a00);
        list.Add(a01);
        list.Add(a10);
        list.Add(a11);
    }
    public ConteinerQuarters()
    {

        list = new List<bool[,]>();
    }
    bool isUpdateOnce = false;
    public void UpdateList() {
        if (isUpdateOnce == false)
        {
            isUpdateOnce = true;
            list.Add(a00);
            list.Add(a01);
            list.Add(a10);
            list.Add(a11);
        }
    }
}

class ResultatsSearchGroupInEachSideQuad {
    public List<List<int>> downLine, rightLine, upLine, leftLine;

    public ResultatsSearchGroupInEachSideQuad(int deapthSearch)
    {
        downLine = new List<List<int>>();
        rightLine = new List<List<int>>();
        upLine = new List<List<int>>();
        leftLine = new List<List<int>>();

        DeapthSearch = deapthSearch;
    }

    public int DeapthSearch { get; set; }
}

class QuadSearchWayPoins {
    public ConteinerQuadSearch quadPickConteiner;
    public ResultatsSearchGroupInEachSideQuad quadResultOut;

    #region constructor, init
    static ResultatsSearchGroupInEachSideQuad CreateResultOut(int deapth) {
        return new ResultatsSearchGroupInEachSideQuad(deapth);
    }

    public QuadSearchWayPoins(bool[,] array, int deapth) : this(array, CreateResultOut(deapth)) { }

    public QuadSearchWayPoins(bool[,] array, ResultatsSearchGroupInEachSideQuad resultOut) {
        SearchRecursia(array, resultOut);
    }
    #endregion

    void SearchRecursia(bool[,] array, ResultatsSearchGroupInEachSideQuad resultOut) {
        if (resultOut != null && resultOut.DeapthSearch > 0)
        {
            resultOut.DeapthSearch--;

            ConteinerQuarters conteinerQuarters = new ConteinerQuarters();

            SplittingQuadIntoQuartersWrapper(array, conteinerQuarters);
            conteinerQuarters.UpdateList();

            ConteinerQuadSearch conteinerQuadSearch = new ConteinerQuadSearch();

            foreach (var item in conteinerQuarters.list)
            {
                ResultatsSearchGroupInEachSideQuad result = CheckSidesQuad(item, resultOut.DeapthSearch);
                conteinerQuadSearch.list.Add(new QuadSearchWayPoins(item, result));
            }
        }
    }

    void SplittingQuadIntoQuartersWrapper(bool[,] array, ConteinerQuarters conteinerQuarters) {
        SplittingQuadIntoQuarters(
            array,
            out conteinerQuarters.a00,
            out conteinerQuarters.a01,
            out conteinerQuarters.a10,
            out conteinerQuarters.a11
        );
    }

    public void SplittingQuadIntoQuarters(bool[,] array, out bool[,] a00, out bool[,] a01, out bool[,] a10, out bool[,] a11) {
        int count_horizontal = array.GetLength(0);
        int count_vertical = array.GetLength(1);

        int mediana_horizontal = count_horizontal / 2;
        int mediana_vertical = count_vertical / 2;

        a00 = new bool[mediana_horizontal, mediana_vertical];
        a01 = new bool[mediana_horizontal, count_vertical - mediana_vertical];
        a10 = new bool[count_horizontal - mediana_horizontal, mediana_vertical];
        a11 = new bool[count_horizontal - mediana_horizontal, count_vertical - mediana_vertical];

        for (int i = 0; i < count_horizontal; i++)
        {
            for (int j = 0; j < count_vertical; j++)
            {
                if (i >= 0 && i < mediana_horizontal)
                {
                    if (j >= 0 && j < mediana_vertical)
                    {
                        a00[i, j] = array[i, j];
                    }
                    else
                    {
                        a01[i, j - mediana_vertical] = array[i, j];
                    }
                }
                else
                {
                    if (j >= 0 && j < mediana_vertical)
                    {
                        a10[i - mediana_horizontal, j] = array[i, j];
                    }
                    else
                    {
                        a11[i - mediana_horizontal, j - mediana_vertical] = array[i, j];
                    }
                }
            }
        }
    }

    ResultatsSearchGroupInEachSideQuad CheckSidesQuad(bool[,] array, int deapthSearch)
    {
        ResultatsSearchGroupInEachSideQuad quadResultOut = new ResultatsSearchGroupInEachSideQuad(deapthSearch);

        CheckSide(array, quadResultOut.downLine, Vector2Int.down);
        CheckSide(array, quadResultOut.rightLine, Vector2Int.right);
        CheckSide(array, quadResultOut.upLine, Vector2Int.up);
        CheckSide(array, quadResultOut.leftLine, Vector2Int.left);

        return quadResultOut;
    }

    List<List<int>> CheckSide(bool[,] arrayQuad, List<List<int>> list, Vector2Int side)
    {
        bool isVertical = false;

        if (Mathf.Abs(side.x) > Mathf.Abs(side.y))
        {
            isVertical = true;
        }

        if (side.x < 0)
        {
            side.Set(0, side.y);
        }
        else if (side.x > 1) {
            side.Set(1, side.y);
        }

        if (side.y < 0)
        {
            side.Set(side.x, 0);
        }
        else if (side.y > 1)
        {
            side.Set(side.x, 1);
        }
        
        bool isContinue = false;
        int countSide = arrayQuad.GetLength(isVertical ? 1 : 0);

        for (int i = 0; i < countSide; i++)
        {

            int xI = (isVertical) ? 0 : i;
            int yI = (isVertical) ? i : 0;

            bool flag = arrayQuad[side.x * (arrayQuad.GetLength(0) - 1) + xI, side.y * (arrayQuad.GetLength(1) - 1) + yI];

            if (flag)
            {
                if (isContinue == false)
                {
                    list.Add(new List<int>());
                }
                list[list.Count - 1].Add(i);

            }
            else
            {
                if (isContinue)
                {
                    isContinue = false;
                }
            }
        }

        return list;
    }
}
