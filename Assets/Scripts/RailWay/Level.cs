﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectTest
{
    class Level : MonoBehaviour
    {
        private RailWay railWay;
        private Train train;
        private List<Stantion> listStantion;

        #region Events
        public event Action<Train, bool> eventUpdatePositionTrain;
        void EventUpdatePosition(Train train, bool isMove) {
            eventUpdatePositionTrain?.Invoke(train, isMove);
        }

        public event Action<StantionDetection, bool> eventStantionDetection;
        void EventStantionDetection(StantionDetection stantion, bool isArea)
        {
            eventStantionDetection?.Invoke(stantion, isArea);
        }
        #endregion
        #region Methods
        public void Restart()
        {
            train.Restart();
        }

        public void InputControlObject(DataActionTrain dataActionTrain)
        {
            train.SelectAction(dataActionTrain);
        }

        public void EventUpdateFocusAttach()
        {
            EventUpdatePosition(train, false);
        }
        #endregion
        #region Init, Set
        public void Init(ManagerGame managerGame)
        {
            CreateGround();
            CreateRailWay();
            CreateStantions();
            CreateTrain();
            AttachTrainToRailWay();
            SetFocusObject();
            SetEventStantionDetection();
        }

        private void SetFocusObject()
        {
            train.eventUpdatePosition += EventUpdatePosition;
        }

        private void SetEventStantionDetection() {
            train.eventStantionDetection += EventStantionDetection;
        }

        private void AttachTrainToRailWay()
        {
            train.AttachTo(railWay.pathCreator);
        }
        #endregion
        #region Update, LateUpdate
        internal void UpdateCustom()
        {
            train?.UpdateCustom();
        }
        internal void LateUpdateCustom()
        {
            train?.LateUpdateCustom();
        }
        #endregion
        #region Create Level
        void CreateGround() {
            this.gameObject.transform.localPosition -= 0.02f * Vector3.up;
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Plane);
            Destroy(go.GetComponent<MeshCollider>());
            go.name = "groundPlane";
            go.SetParentPrototype(this);
            go.SetMaterialPrototype("#068706");
        }

        void CreateRailWay() {
            GameObject go = new GameObject("railWay");
            go.SetParentPrototype(this);
            railWay = go.AddComponent<RailWay>();
            railWay.Init();
        }

        private void CreateStantions() {
            List<Stantion> listStantion = new List<Stantion>();
            for (int i = 0; i < GameDefines.Instance.Data.COUNT_STANTION; i++)
            {
                Stantion stantion = CreateStantion(i);
                listStantion.Add(stantion);
            }
        }

        private Stantion CreateStantion(int index)
        {
            GameObject go = new GameObject("stantion_" + index);
            go.SetParentPrototype(this);
            Transform tr = go.transform;
            float distance = railWay.pathCreator.path.length * (float)index / (float)GameDefines.Instance.Data.COUNT_STANTION;
            
            Vector3 point = railWay.pathCreator.path.GetPointAtDistance(distance);
            tr.position = point;
            Vector3 direction = railWay.GetDirectionAtDistance(distance);
            tr.rotation = Quaternion.LookRotation(direction);
            Stantion stantion = go.AddComponent<Stantion>();
            stantion.Init(index);

            return stantion;
        }

        private void CreateTrain()
        {
            GameObject go = new GameObject("train");
            go.SetParentPrototype(this);
            train = go.AddComponent<Train>();
            train.Init();
        }
        #endregion
    }
}