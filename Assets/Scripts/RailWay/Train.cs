﻿using UnityEngine;
using System.Collections;
using System;
using PathCreation;

namespace ProjectTest
{
    public class Train : MonoBehaviour
    {
        public PathCreator pathCreator;
        float dstTravelled;
        float speed_current = 0f;
        float speed_max = 1f;

        public float SpeedCurrent {
            private set { }
            get {
                return speed_current;
            }
        }

        bool isStay = true;
        float tracktion = 0f;
        AudioSource audioSource;
        DetectionCollider detectionCollider;

        Transform transformTrain;

        public Transform TransformTrain
        {
            get
            {
                return transformTrain;
            }
            private set
            {
                transformTrain = value;
            }
        }
        bool IsStay
        {
            get
            {
                return isStay;
            }
            set
            {
                if (value != isStay)
                {
                    isStay = value;
                    if (value)
                    {
                        EventUpdatePostion(false);
                    }
                }
            }
        }

        public event Action<StantionDetection, bool> eventStantionDetection;
        void EventStantionDetection(StantionDetection stantion, bool isArea)
        {
            eventStantionDetection?.Invoke(stantion, isArea);
        }

        public event Action<Train, bool> eventUpdatePosition;
        void EventUpdatePostion(bool isMove)
        {
            eventUpdatePosition?.Invoke(this, isMove);
        }

        #region Methods Action
        public void SelectAction(DataActionTrain dataActionTrain) {
            if (dataActionTrain == GameDefines.Instance.Data.TRAIN_GO)
            {
                tracktion = 1f;
            }
            else if (dataActionTrain == GameDefines.Instance.Data.TRAIN_STOP)
            {
                tracktion = 0f;
            }
            else if (dataActionTrain == GameDefines.Instance.Data.TRAIN_BEEP)
            {
                Beep();
            }
        }

        void Beep() {
            if (audioSource.isPlaying == false)
            {
                audioSource.Play();
            }
        }

        public void AttachTo(PathCreator pathCreator)
        {
            this.pathCreator = pathCreator;
            SetTransfrom(dstTravelled);
        }

        public void Restart()
        {
            speed_current = 0f;
            tracktion = 0f;
            dstTravelled = 0f;
            IsStay = true;
            SetTransfrom(0f);
            EventUpdatePostion(false);
        }
        #endregion
        #region Init, Create
        public void Init()
        {
            transformTrain = this.transform;
            audioSource = this.gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.clip = (AudioClip)Resources.Load(GameDefines.Instance.Data.STR_PATH_SOUND_BEEP);
            Rigidbody rigidBody = this.gameObject.AddComponent<Rigidbody>();
            rigidBody.isKinematic = true;
            rigidBody.useGravity = false;
            CreateView();
            CreateCollider();
        }

        void CreateView()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Destroy(go.GetComponent<BoxCollider>());
            go.name = "ViewTrain";
            go.SetParentPrototype(this);
            go.transform.localScale = new Vector3(0.1f, 0.1f, 0.5f);
            go.transform.localPosition += .05f * Vector3.up;
            go.SetMaterialPrototype(Color.red);
        }
        void CreateCollider()
        {
            GameObject go = new GameObject("ColliderDetection");
            go.SetParentPrototype(this);
            go.transform.localScale = new Vector3(0.1f, 0.1f, 0.5f);
            go.transform.localPosition += .05f * Vector3.up;
            BoxCollider boxCollider = go.AddComponent<BoxCollider>();
            boxCollider.isTrigger = true;
            detectionCollider = go.AddComponent<DetectionCollider>();
            detectionCollider.eventStantionDetection += EventStantionDetection;
        }
        #endregion
        #region Update, LateUpdate, SetTransform
        public void LateUpdateCustom()
        {
            if (speed_current > 0f)
            {
                if (isStay)
                {
                    IsStay = false;
                }
                EventUpdatePostion(true);
            }
        }
        public void UpdateCustom()
        {
            if (pathCreator != null)
            {
                
                bool flag = Library.ToValue(1f, speed_max * tracktion, ref speed_current);

                if (speed_current > 0f)
                {

                    dstTravelled += speed_current * Time.deltaTime;

                    SetTransfrom(dstTravelled);
                }
                else if (flag && isStay == false) {
                    IsStay = true;
                }
            }
        }
        void SetTransfrom(float dstTrabelled)
        {
            transformTrain.position = pathCreator.path.GetPointAtDistance(dstTravelled);
            transformTrain.rotation = Quaternion.LookRotation(pathCreator.path.GetDirectionAtDistance(dstTravelled));
        }
        #endregion
    }
}