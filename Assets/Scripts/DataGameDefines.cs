﻿using UnityEngine;
using System.Collections;
using System;

namespace ProjectTest
{

    [CreateAssetMenu(menuName = "ProjectTest/Create dataGameDefines", fileName = "dataGameDefines")]
    public class DataGameDefines : ScriptableObject
    {
        public Font fontDefault;
        public float TIME_ROUND_GAME;
        public string STR_PATH_IMAGE_MAP;
        public string STR_PATH_SOUND_BEEP; 
        public DataButton BUTTON_GO; 
        public DataButton BUTTON_BEEP; 
        public DataButton BUTTON_TAKE_CARGO; 
        public DataButton BUTTON_RESTART; 
        public DataButton BUTTON_SWITCH_CAMERA; 
        public int COUNT_STANTION; 
        public DataActionTrain TRAIN_STOP; 
        public DataActionTrain TRAIN_GO; 
        public DataActionTrain TRAIN_BEEP; 
        public DataGUISet GUI_PLAY; 
        public DataGUISet GUI_RESTART;
        public DataCamera CAMERA_FREE;
        public DataCamera CAMERA_FIXED;
        public DataStateGame GAME_PLAY;
        public DataStateGame GAME_PAUSE;
        public DataStateGame GAME_OVER;
        public TypeIndicationValue INDICATION_SCORE;
        public TypeIndicationValue INDICATION_TIMER;

        public Vector3 OFFSET_STANTION_BUILDER;

        public float SCALE_FACTOR;
        public Vector2 RESOLUTION;

    }

}