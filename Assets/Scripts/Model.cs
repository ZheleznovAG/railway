﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

namespace ProjectTest
{
    [Serializable]
    public class Model
    {
        public int scoreCurrent;
        public int scoreBest;
        public DataStateGame stateGame;
        public DataCamera stateCamera;
        public float speedTrain;
        public float timer;

        StringBuilder str_timer;
        int _timer_minutes_last = 0;
        int _timer_seconds_last = 0;

        bool isTrainStay = true;
        bool isObjectInAreaStantion = false;
        bool isOnceTakeCargo = false;
        bool isAvailableAciton = false;

        public Model() {
            stateGame = GameDefines.Instance.Data.GAME_PLAY;
            str_timer = new StringBuilder(10);
            ResetTimer();
        }
        public void UpdateIndicationInit() {
            EventUpdateIndicationValue(scoreCurrent + "", GameDefines.Instance.Data.INDICATION_SCORE);
        }

        public event Action<bool> eventIsAvailable;
        void EventIsAvailableTakeCargo(bool isAvailable)
        {
            eventIsAvailable?.Invoke(isAvailable);
        }

        public event Action eventOverTime;
        void EventOverTimer()
        {
            eventOverTime?.Invoke();
        }

        public event Action<string, TypeIndicationValue> eventUpdateIndicationValue;
        void EventUpdateIndicationValue(string value, TypeIndicationValue typeIndicationValue) {
            eventUpdateIndicationValue?.Invoke(value, typeIndicationValue);
        }

        public void ResetTimer() {
            timer = GameDefines.Instance.Data.TIME_ROUND_GAME;
        }

        int timer_minutes_last {
            set
            {
                _timer_minutes_last = value;
                SendStringTimerIndication();
            }
            get {
                return _timer_minutes_last;
            }
        }
        int timer_seconds_last {
            set {
                _timer_seconds_last = value;
                SendStringTimerIndication();
            }
            get {
                return _timer_seconds_last;
            }
        }

        void SendStringTimerIndication()
        {
            str_timer.Remove(0, str_timer.Length);
            str_timer.Append(timer_minutes_last);
            str_timer.Append(':');
            if (timer_seconds_last < 10)
            {
                str_timer.Append(0);
            }
            str_timer.Append(timer_seconds_last);

            EventUpdateIndicationValue(str_timer + "", GameDefines.Instance.Data.INDICATION_TIMER);
        }

        public void UpdateTimer()
        {
            timer -= Time.deltaTime;

            if (timer <= 0f)
            {
                timer = 0f;
                stateGame = GameDefines.Instance.Data.GAME_OVER;
                EventOverTimer();
            }

            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);

            if (minutes != timer_minutes_last)
            {
                timer_minutes_last = minutes;

            }
            if (seconds != timer_seconds_last)
            {
                timer_seconds_last = seconds;

            }
        }

        public float SpeedTrain {
            set {
                speedTrain = value;
                if (Mathf.Abs(value) < float.Epsilon)
                {
                    IsTrainStay = true;
                }
                else if(IsTrainStay) {
                    IsTrainStay = false;
                }
            }
            get {
                return speedTrain;
            }
        }

        bool IsTrainStay {
            get {
                return isTrainStay;
            }
            set {
                if (isTrainStay != value)
                {
                    isTrainStay = value;
                    CheckIsAvailableAction();
                }
            }
        }

        public int ScoreCurrent {
            set {
                if (scoreCurrent != value)
                {
                    scoreCurrent = value;
                    EventUpdateIndicationValue(value + "", GameDefines.Instance.Data.INDICATION_SCORE);
                }
            }
            get {
                return scoreCurrent;
            }
        }

        public void UpdatePositionTrain(Train train, bool isMove)
        {
            if (isMove && IsTrainStay)
            {
                IsTrainStay = false;
            } else if (isMove == false && IsTrainStay == false)
            {
                IsTrainStay = true;
            }
        }

        public void ObjectInAreaStantion(StantionDetection stantion, bool isArea)
        {
            if (isArea && IsObjectInAreaStantion == false)
            {
                IsObjectInAreaStantion = true;
                CheckIsAvailableAction();
            }
            else if (isArea == false && IsObjectInAreaStantion)
            {
                IsObjectInAreaStantion = false;
            }
        }

        internal void Restart()
        {
            ResetTimer();
            ScoreCurrent = 0;
            stateGame = GameDefines.Instance.Data.GAME_PLAY;
            CheckIsAvailableAction();
        }

        bool IsObjectInAreaStantion {
            get {
                return isObjectInAreaStantion;
            }
            set {
                isObjectInAreaStantion = value;
                isOnceTakeCargo = value;
            }
        }

        void CheckIsAvailableAction() {
            if (IsObjectInAreaStantion && IsTrainStay && isOnceTakeCargo)
            {
                IsAvailableAction = true;
            }
            else {
                IsAvailableAction = false;
            }
        }

        public bool IsAvailableAction {
            get { return isAvailableAciton; }
            internal set {
                if (isAvailableAciton != value)
                {
                    isAvailableAciton = value;
                    EventIsAvailableTakeCargo(value);
                }
            }
        }

        internal void TakeCargo()
        {
            if (isOnceTakeCargo)
            {
                isOnceTakeCargo = false;
                CheckIsAvailableAction();
                ScoreCurrent++;
            }
        }
    }
}