﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create dataStateGame", fileName = "data_stat_game")]
    public class DataStateGame : ScriptableObject
    {
        public bool isResetControls;
        public bool isPause;
        public bool isResetGame;

    }
}