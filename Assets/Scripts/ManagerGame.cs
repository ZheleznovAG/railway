﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTest
{

    public class ManagerGame : MonoBehaviour
    {
        Level level;
        ManagerGUI managerGUI;
        ManagerCamera managerCamera;
        EventSystem eventSystem;
        InputControl inputControl;
        Logic logic;

        void Start()
        {
            LoadData();
            CreateEventSystem();
            CreateLevel();
            CreateGUI();
            CreateCamera();
            CreateLogic();
        }

        void LoadData()
        {
            GameDefines.Instance.Init();
        }

        private void CreateEventSystem()
        {
            eventSystem = new GameObject("EventSystem").AddComponent<EventSystem>();
            eventSystem.gameObject.AddComponent<StandaloneInputModule>();
        }

        void CreateLevel()
        {
            level = new GameObject("level").AddComponent<Level>();
            level.SetParentPrototype(this);
            level.Init(this);
        }

        void CreateGUI()
        {
            managerGUI = new GameObject("GUI").AddComponent<ManagerGUI>();
            managerGUI.Init();
            inputControl = managerGUI.InputControl;
        }

        void CreateCamera()
        {
            managerCamera = new GameObject("Cameras").AddComponent<ManagerCamera>();
            managerCamera.Init();
        }

        void CreateLogic() {
            logic = new Logic(level, managerGUI, inputControl, managerCamera);
        }

        void Update() {
            logic?.UpdateCustom();
        }
        void LateUpdate() {
            logic?.LateUpdateCustom();
        }
    }

}