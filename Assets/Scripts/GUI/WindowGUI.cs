﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace ProjectTest {

    public class WindowGUI : ItemGUI
    {
        List<ButtonCustom> buttons = new List<ButtonCustom>();
        List<IndicationValue> indication = new List<IndicationValue>();

        public DataGUISet Data
        {
            private set; get;
        }

        public override void Reset() {
            IsVisible = false;
        }

        #region Create and Init
        public void Init(DataGUISet data) {
            base.Init();
            Data = data;
        }

        internal void AddIndication(IndicationValue indicationValue)
        {
            indication.Add(indicationValue);
        }

        internal void AddButton(ButtonCustom buttonCustom)
        {
            buttons.Add(buttonCustom);
        }
        #endregion
    }
}