﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectTest
{
    public class InputControl : MonoBehaviour
    {
        public LookPad lookPad;
        bool _isEnabled = false;

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                lookPad.IsEnabled = value;

                _isEnabled = value;
            }
        }

        public void Init(LookPad lookPad)
        {
            this.lookPad = lookPad;
            lookPad.eventLook += EventInputLook;
        }

        public void Reset()
        {
            lookPad.Reset();
        }

        public event Action<DataButton, bool> eventClickButton;
        public event Action<Vector2> eventInputLook;

        public void ClickButton(DataButton dataTypeButton, bool isPressed) {
            eventClickButton?.Invoke(dataTypeButton, isPressed);
        }

        void EventInputLook(Vector2 vector) {
            eventInputLook?.Invoke(vector);
        }
    }
}