﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data indication value", fileName = "data_indicationValue_")]
    public class DataIndicationValue : ScriptableObject
    {
        public TypeIndicationValue type;
        public Vector2 position;
        public AnchorPresets anchorPresets;
        public PivotPresets pivotPresets;
        public Vector2 size;
        public bool isDefaultVisible = true;
    }
}