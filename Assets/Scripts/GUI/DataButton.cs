﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data button", fileName = "data_button_")]
    public class DataButton : ScriptableObject
    {
        [SerializeField] string _text = "";
        [SerializeField] Vector2 _position;
        [SerializeField] AnchorPresets _anchorPresets;
        [SerializeField] PivotPresets _pivotPresets;
        [SerializeField] Vector2 _size;

        public bool isDefaultVisible = true;

        public string text { get { return _text; } }
        public Vector2 position { get { return _position; } }
        public AnchorPresets anchorPresets { get { return _anchorPresets; } }
        public PivotPresets pivotPresets { get { return _pivotPresets; } }
        public Vector2 size { get { return _size; } }
    }
}