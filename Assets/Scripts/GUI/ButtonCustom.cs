﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ProjectTest {

    public class ButtonCustom : ItemGUI, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
    {
        Button button;
        Image image;

        public bool IsPressed {
            private set; get;
        }
        public DataButton Type
        {
            private set; get;
        }
        public override void Reset() {
            IsPressed = false;
            IsVisible = false;
        }

        #region Event Pressed, OnPointerUp, OnPointerDown, OnPointerExit
        public event Action<DataButton, bool> eventPressed;
        void EventPressed(bool isPressed)
        {
            eventPressed?.Invoke(Type, isPressed);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (IsPressed)
            {
                EventPressed(false);
                IsPressed = false;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (IsPressed)
            {
                EventPressed(false);
                IsPressed = false;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (IsPressed == false)
            {
                EventPressed(true);
                IsPressed = true;
            }
        }
        #endregion
        #region Create and Init ButtonCustom
        public void Init(DataButton type) {
            base.Init();
            Type = type;

            CreateComponents();
        }
        void CreateComponents() {

            RectTransform rc = this.gameObject.AddComponent<RectTransform>();
            rc.SetPivot(Type.pivotPresets);
            rc.SetAnchor(Type.anchorPresets);
            rc.anchoredPosition = Type.position;
            rc.sizeDelta = Type.size;

            image = this.gameObject.AddComponent<Image>();

            button = this.gameObject.AddComponent<Button>();

            CreateLabelButton(Type);
        }

        void CreateLabelButton(DataButton _dataTypeButton)
        {
            GameObject go = new GameObject("button_text");
            go.SetParentPrototype(this.gameObject);
            RectTransform rc = go.AddComponent<RectTransform>();
            rc.SetAnchor(AnchorPresets.StretchAll);
            rc.SetOffsetZero();
            Text text = go.AddComponent<Text>();
            text.text = _dataTypeButton.text;
            text.supportRichText = false;
            text.font = GameDefines.Instance.Data.fontDefault;
            text.fontSize = 30;
            text.color = Color.black;
            text.alignment = TextAnchor.MiddleCenter;
        }
        #endregion
    }
}