﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ProjectTest
{
    public class LookPad : MonoBehaviour
    {
        public event Action<Vector2> eventLook;

        Vector2 touchInput, prevDelta, dragInput;
        bool _isEnabled = false;
        bool isPressed;
        EventTrigger eventTrigger;

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (value)
                {
                    Reset();
                }
                else
                {
                    Reset();
                }

                _isEnabled = value;
            }
        }

        public void Reset()
        {
            dragInput = Vector2.zero;
            touchInput = Vector2.zero;
            prevDelta = Vector2.zero;
            isPressed = false;
        }

        #region Init and Create Components
        public void Init()
        {
            CreateUIComponents();
            CreateEventTrigger();
            SetupListeners();
        }

        void CreateUIComponents()
        {
            RectTransform rt = this.gameObject.AddComponent<RectTransform>();
            rt.SetAnchor(AnchorPresets.StretchAll);
            rt.SetOffsetZero();

            Image image = this.gameObject.AddComponent<Image>();
            image.color = Color.clear;
        }

        void CreateEventTrigger()
        {
            eventTrigger = this.gameObject.AddComponent<UnityEngine.EventSystems.EventTrigger>();
        }

        void SetupListeners()
        {
            eventTrigger = gameObject.GetComponent<EventTrigger>();

            var pointerDown = new EventTrigger.TriggerEvent();
            var drag = new EventTrigger.TriggerEvent();
            var endDrag = new EventTrigger.TriggerEvent();

            pointerDown.AddListener(data =>
            {
                var evData = (PointerEventData)data;

                data.Use();
                if (IsEnabled)
                {
                    isPressed = true;
                    prevDelta = dragInput = evData.position;
                }
            });

            drag.AddListener(data =>
            {
                var evData = (PointerEventData)data;

                data.Use();
                if (IsEnabled && isPressed)
                {
                    dragInput = evData.position;
                    UpdateCustom();
                }
                
            });

            endDrag.AddListener(data =>
            {
                data.Use();
                if (IsEnabled && isPressed)
                {
                    Reset();
                }
            });

            eventTrigger.triggers.Add(new EventTrigger.Entry { callback = pointerDown, eventID = EventTriggerType.PointerDown });
            eventTrigger.triggers.Add(new EventTrigger.Entry { callback = drag, eventID = EventTriggerType.Drag });
            eventTrigger.triggers.Add(new EventTrigger.Entry { callback = endDrag, eventID = EventTriggerType.EndDrag });
        }
        #endregion
        #region Update data and Send
        void UpdateCustom()
        {
            if (_isEnabled)
            {
                touchInput = (dragInput - prevDelta) / Time.deltaTime;
                prevDelta = dragInput;
                EventLook(touchInput);
            }
        }

        void EventLook(Vector2 value)
        {
            eventLook?.Invoke(value);
        }
        #endregion
    }
}