﻿using UnityEngine;

namespace ProjectTest
{
    public class ItemGUI : MonoBehaviour
    {
        private CanvasGroup cg;
        bool isVisible;

        protected void Init()
        {
            cg = this.gameObject.AddComponent<CanvasGroup>();

            IsVisible = false;
        }

        public virtual bool IsVisible
        {
            set
            {
                isVisible = value;
                if (value)
                {
                    cg.alpha = 1f;
                    cg.interactable = true;
                    cg.blocksRaycasts = true;
                }
                else
                {
                    cg.alpha = 0f;
                    cg.interactable = false;
                    cg.blocksRaycasts = false;
                }
            }
            get
            {
                return isVisible;
            }
        }

        public virtual void Reset() { }
    }
}