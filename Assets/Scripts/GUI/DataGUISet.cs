﻿using UnityEngine;
using System.Collections.Generic;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data GUI Set", fileName = "data_guiset_")]
    public class DataGUISet : ScriptableObject
    {
        public bool isTouchEnable;
        public DataButton[] buttons;
        public DataIndicationValue[] indicationValue;
    }
}