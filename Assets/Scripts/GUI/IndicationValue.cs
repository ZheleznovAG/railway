﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ProjectTest {

    public class IndicationValue : ItemGUI
    {
        Text text;
        public string Value
        {
            set {
                text.text = value;
            }
        }
        public TypeIndicationValue Type
        {
            private set; get;
        }
        public DataIndicationValue Data
        {
            private set; get;
        }
        public override void Reset() {
            IsVisible = false;
        }

        #region Create and Init ButtonCustom
        public void Init(DataIndicationValue data) {
            base.Init();
            Data = data;
            Type = data.type;

            CreateBaseComponent();
            CreateTextComponent();
        }
        void CreateBaseComponent() {

            RectTransform rc = this.gameObject.AddComponent<RectTransform>();
            rc.SetPivot(Data.pivotPresets);
            rc.SetAnchor(Data.anchorPresets);
            rc.anchoredPosition = Data.position;
            rc.sizeDelta = Data.size;
            Image image = rc.gameObject.AddComponent<Image>();
            image.color = Color.blue;
            image.color = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.3f);
        }
        void CreateTextComponent()
        {
            GameObject go = new GameObject("indication_text");
            go.SetParentPrototype(this.gameObject);
            RectTransform rc = go.AddComponent<RectTransform>();
            rc.SetAnchor(AnchorPresets.StretchAll);
            rc.SetOffsetZero();
            text = go.AddComponent<Text>();
            text.supportRichText = false;
            text.font = GameDefines.Instance.Data.fontDefault;
            text.fontSize = 60;
            text.color = Color.white;
            text.alignment = TextAnchor.MiddleCenter;
        }
        #endregion
    }
}