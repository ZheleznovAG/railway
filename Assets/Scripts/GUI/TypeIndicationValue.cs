﻿using UnityEngine;

namespace ProjectTest
{
    [CreateAssetMenu(menuName = "ProjectTest/Create data type indication value", fileName = "data_typeIndication_")]
    public class TypeIndicationValue : ScriptableObject
    {
        public string text = "";
    }
}