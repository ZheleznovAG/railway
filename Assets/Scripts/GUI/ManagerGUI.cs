﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

namespace ProjectTest
{
    class ManagerGUI : MonoBehaviour
    {
        InputControl inputControl;
        Canvas canvas;
        CanvasScaler canvasScaler;
        GraphicRaycaster graphicRaycaster;

        WindowGUI windowPLAY;
        WindowGUI windowRESTART;

        List<ButtonCustom> listButtons = new List<ButtonCustom>();
        List<IndicationValue> listIndications = new List<IndicationValue>();

        public InputControl InputControl {
            private set {}
            get {
                return inputControl;
            }
        }

        public event Action<DataButton, bool> eventClickButton;
        void ClickButton(DataButton dataTypeButton, bool isPressed)
        {
            eventClickButton?.Invoke(dataTypeButton, isPressed);
        }

        public void IsShowHideItem(bool isShow, DataButton dataButton) {
            var item = from button in listButtons
                       where button.Type == dataButton
                       select button;

            item.FirstOrDefault().IsVisible = isShow;
        }

        WindowGUI windowGUILast;
        public void ToWindow(DataGUISet dataGUISet) {
            InputControl.IsEnabled = dataGUISet.isTouchEnable;
            if (windowGUILast != null)
            {
                windowGUILast.IsVisible = false;
            }
            if (dataGUISet == GameDefines.Instance.Data.GUI_PLAY)
            {
                windowGUILast = windowPLAY;
            }
            else if (dataGUISet == GameDefines.Instance.Data.GUI_RESTART)
            {
                windowGUILast = windowRESTART;
            }

            windowGUILast.IsVisible = true;
        }

        public void UpdateIndicationValue(string value, TypeIndicationValue typeIndicationValue) {
            foreach (var item in listIndications)
            {
                if (item.Type == typeIndicationValue)
                {
                    item.Value = value;
                }
            }
        }

        public void AttachCameraToGUI(Camera camera)
        {
            canvas.worldCamera = camera;
        }

        #region Init and Create GUI
        public void Init()
        {
            this.gameObject.layer = LayerMask.NameToLayer("UI");
            CreateGUI();
            ToWindow(GameDefines.Instance.Data.GUI_PLAY);
        }

        void CreateGUI() {
            CreateCanvas();
            CreateInputControl();
            CreateWindows();
        }

        void CreateCanvas()
        {
            canvas = this.gameObject.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceCamera;

            graphicRaycaster = this.gameObject.AddComponent<GraphicRaycaster>();

            canvasScaler = this.gameObject.AddComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            canvasScaler.referenceResolution = GameDefines.Instance.Data.RESOLUTION;
            canvasScaler.scaleFactor = GameDefines.Instance.Data.SCALE_FACTOR;
            
        }

        void CreateInputControl()
        {
            inputControl = this.gameObject.AddComponent<InputControl>();

            GameObject go = new GameObject("LookPad");
            go.SetParentPrototype(this);
            LookPad lookPad = go.AddComponent<LookPad>();
            lookPad.Init();
            lookPad.IsEnabled = true;
            inputControl.Init(lookPad);

            eventClickButton += inputControl.ClickButton;
        }

        void CreateWindows() {
            windowPLAY = CreateWindow(GameDefines.Instance.Data.GUI_PLAY);
            windowRESTART = CreateWindow(GameDefines.Instance.Data.GUI_RESTART);
        }

        WindowGUI CreateWindow(DataGUISet dataGUISet) {
            GameObject go = new GameObject("Window_" + dataGUISet);
            go.SetParentPrototype(this);
            WindowGUI windowGUI = go.AddComponent<WindowGUI>();
            windowGUI.Init(dataGUISet);

            RectTransform rc = go.AddComponent<RectTransform>();
            rc.SetPivot(PivotPresets.MiddleCenter);
            rc.SetAnchor(AnchorPresets.StretchAll);
            rc.SetOffsetZero();

            foreach (var item in dataGUISet.buttons)
            {
                CreateButton(item, windowGUI);
            }

            foreach (var item in dataGUISet.indicationValue)
            {
                CreateIndication(item, windowGUI);
            }

            return windowGUI;
        }

        void CreateIndication(DataIndicationValue dataIndicationValue, WindowGUI windowGUI)
        {
            GameObject go = new GameObject(dataIndicationValue.name);
            go.SetParentPrototype(windowGUI);
            IndicationValue indicationValue = go.AddComponent<IndicationValue>();
            indicationValue.Init(dataIndicationValue);
            windowGUI.AddIndication(indicationValue);
            indicationValue.IsVisible = dataIndicationValue.isDefaultVisible;
            listIndications.Add(indicationValue);
        }

        void CreateButton(DataButton _dataTypeButton, WindowGUI windowGUI)
        {
            GameObject go = new GameObject(_dataTypeButton.name);
            go.SetParentPrototype(windowGUI);
            ButtonCustom buttonCustom = go.AddComponent<ButtonCustom>();
            buttonCustom.Init(_dataTypeButton);
            windowGUI.AddButton(buttonCustom);
            buttonCustom.IsVisible = _dataTypeButton.isDefaultVisible;

            buttonCustom.eventPressed += ClickButton;

            listButtons.Add(buttonCustom);
        }
        #endregion
    }
}